'''
Created on Jun 17, 2020
@author: Amy Renne
'''
import numpy as np
import math
import matplotlib.pyplot as plt

"""
Defines variables
"""
dl = 0.01
emmitance= 0.00001 
alpha = 1.99784922429243
beta = 13.2040914003447
gamma = 0.378019310202258
k = 3
theta = math.sqrt(k)*dl
z_total = np.arange(0, 1.25, 0.01).tolist()
z_drift = np.arange(0.25, 1.25, 0.01).tolist()
z_quad = np.arange(0, 0.25, 0.01).tolist()

"""
Defines matrices
"""
#Drift Matrix ~ Drifts length dl
D = np.array([[1, dl], [0, 1]])
#Quad Matrix 
Q = np.array([[ math.cos(theta), (1/math.sqrt(k))*(math.sin(theta))], 
              [(-(math.sqrt(k)))*(math.sin(theta)), math.cos(theta)]])
#Sigma Matrix
sigma = np.array([[emmitance*beta, -emmitance*alpha], [-emmitance*alpha, emmitance*gamma]])
#Map Matrix
M = D.dot(Q)
print("Map: " + str(M))

""" 
Begins loop to obtain plot values
"""
x = []
x_square = []
i = 0
j = 25

#Quad
for i in range(0, 25):
    sigma = Q.dot(sigma.dot(Q.transpose()))
    x_var =  math.sqrt((sigma[0][0]))
    x_square_var = (sigma[0][0])
    x.append(x_var)
    x_square.append(x_square_var)
    emmitance_f = math.sqrt((sigma[0][0])*(sigma[1][1]) - (sigma[0][1])**2)
    i += 1
#Drift
for j in range(25, 125):
    sigma = D.dot(sigma.dot(D.transpose()))
    x_var =  math.sqrt((sigma[0][0]))
    x_square_var = (sigma[0][0])
    x.append(x_var)
    x_square.append(x_square_var)
    emmitance_f = math.sqrt((sigma[0][0])*(sigma[1][1]) - (sigma[0][1])**2)
    j += 1

"""
Calculates final values
"""
min_x = min(x)
beta_final = sigma[0][0]/emmitance
alpha_final = -sigma[0][1]/emmitance
gamma_final = sigma[1][1]/emmitance

"""
Outputs final values, plots graph
"""
print("Sigma: " +str(sigma)) 
print('final beta = ' + str(beta_final))   
print('final alpha = ' + str(alpha_final))
print('final gamma = ' + str(gamma_final)) 
print('final emmitance: ' + str(emmitance_f))
print('x_viewer = ' + str(x[124]))
print('x^2 viewer = ' + str(x_square[124]))
plt.plot(z_total, x)
plt.xlabel('z (beamline)')
plt.ylabel('x')
plt.show()


