"""
Created on Mon Jun 29 13:03:10 2020
@author: Amy Renne
"""
from sympy import *
import numpy as np
import math
import matplotlib.pyplot as plt

"""
Defines variables
"""
a = Symbol('a')
b = Symbol('b')
e = Symbol('e')
g = ((1+ a**2)*b**(-1))

#dimensions are in meters for all lengths
L_drift = 1.5
L_quad = 0.0762
k = 2.45


theta = math.sqrt(k)*L_quad

"""
Defines matrixes
"""
#Drift Matrix ~ Drifts length L_drift
D = Matrix([[1, L_drift],[0, 1]])
#Quad Matrix ~ quad legth L_quad 
Q = Matrix([[ math.cos(theta), (1/math.sqrt(k))*(math.sin(theta))], 
              [(-(math.sqrt(k)))*(math.sin(theta)), math.cos(theta)]])
#Original sigma matrix
sigma_i = Matrix([[b, -a], [-a, g]])

"""
Operates on matrixes, creates final sigma
"""
#Map matrix ~ drift -> quad -> waist
M = np.matmul(D,Q)
M_t = np.transpose(M)
#print('Map = ' + str(M))
#Final sigma matrix
sigma_f = np.matmul(M,(np.matmul(sigma_i, M_t)))
#print('Sigma = ' + str(sigma_f))

"""
Calculates component values
"""
sigma_0_0 = sigma_f[0][0]
sigma_1_1 = sigma_f[1][1]
sigma_0_1 = sigma_f[0][1]

"""
Outputs data
"""
print('beta final = ' + str(sigma_0_0))
#print('gamma final = ' + str(sigma_1_1))
print('alpha final = ' + str(sigma_0_1))




